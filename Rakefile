require 'rake'
require 'fileutils'
require 'webrick'

require 'rake/clean'

require 'guard'
require 'guard/commander' # needed because of https://github.com/guard/guard/issues/793

require 'asciidoctor'
require 'asciidoctor/cli'

# TODO: check the output structure
# html must exist with mycss and reveal.js
# make a common commit for sube repos, or incude them

# Rake.application.options.trace_rules = true

PACKNAME = File.basename File.dirname __FILE__
PACKGZ = PACKNAME + '-pack.tar.gz'
PACKZIP = PACKNAME + '-pack.zip'

ROOTDIR = File.dirname(__FILE__)

PACKDIR = 'html'.freeze

ADOCS = Rake::FileList['**/*.adoc'].exclude 'asciidoctor-reveal.js/**/*',
                                            'html/**/*',
                                            'vorlage/**/*'

PRES = ADOCS.select { |x| x.count('_').zero? }
INCLS = ADOCS.reject { |x| x.count('_').zero? }

HTMLDEPS = Hash[PRES.collect do |x|
                  task = x.pathmap '%X'

                  depends = Rake::FileList["#{task}/*.*"]
                  includes = INCLS.select { |inc| inc.start_with? task + '_' }
                  ["#{PACKDIR}/#{task}.html", [x] + includes + depends.to_ary]
                end
]

TASKS = Hash[HTMLDEPS.keys.collect do |x|
               # [(x.sub("html/", "").pathmap("%X")<<".adoc").to_sym, x]
               # creates unfortunately circular dependencies,
               # but would be nice to have dir/file.adoc as task,
               # TODO  possible?
               [x.sub('html/', '').pathmap('%X').to_sym, x]
             end]

# define what happens on clean, don't use CLOBBER now,  maybe in future ..
CLEAN << PACKDIR
CLEAN << PACKZIP if File.exist? PACKZIP

TASKS.each do |taskname, html|
  dir = taskname.to_s.pathmap '%d'

  filedeps = []

  %w[images config].each do |syncdir|
    syncdirpath = File.join dir, syncdir
    next unless Dir.exist? syncdirpath

    outdir = File.join PACKDIR, dir, syncdir
    directory outdir

    dirglob = Dir.glob "#{syncdirpath}/**/*"

    dir_tasks = dirglob.select { |d| File.directory? d }.map do |d|
      File.join PACKDIR, d
    end

    dir_tasks.each { |d| directory d }

    files = Hash[dirglob.select { |f| File.file? f }.map do |f|
                   fpath = File.join PACKDIR, f
                   [f, fpath]
                 end]

    files.each do |src, dest|
      # need hanling copy of html files extra
      # this shall not be handled by the rule, since this has no rule
      if src.pathmap('%x') == '.html'
        task dest do
          unless FileUtils.uptodate?(dest, [src])
            FileUtils.mkdir_p File.dirname(dest)
            cp src, dest, verbose: true
          end
        end
      else
        file dest => src do |task|
          unless FileUtils.uptodate?(task.name, [task.prerequisites.first])
            cp task.prerequisites.first, task.name, verbose: true
          end

        end
      end
    end

    filedeps.concat [outdir]
    filedeps.concat dir_tasks
    filedeps.concat files.values
  end

  task taskname => filedeps + [html] do
  end
end

task default: TASKS.keys

rule '.html' => ->(html) { HTMLDEPS[html] } do |html|
  # hm, that's a bit of overdose, but,
  # I might call rake from above dir
  # or rake -f ../Rakefile
  # or I am in the presentation dir with Rakefile
  # however,
  # if there is a template dir in the presentation folder it shall be usede

  adoc_templates = File.join 'asciidoctor-reveal.js', 'templates'
  current_dir = html.source.pathmap '%d'
  template_dir = File.join current_dir, adoc_templates
  template_dir = File.join '..', adoc_templates unless Dir.exist? template_dir
  template_dir = File.join adoc_templates unless Dir.exist? template_dir

  # if the rake file is in the top dir, we need the prefix
  current_full = File.expand_path current_dir
  relative_p = current_full.delete_prefix ROOTDIR

  docattribs = []
  unless relative_p.empty?
    docattribs << "-a relpath=#{File.join '.', relative_p}"
  end

  options = "-T #{template_dir}"
  options << ' -r asciidoctor-diagram '
  options << docattribs.join(' ').to_s unless docattribs.empty?
  options << " -o #{html.name} #{html.source}"

  puts "Invoke: asciidoctor #{options}"

  invoker = Asciidoctor::Cli::Invoker.new options.split(' ')
  GC.start
  invoker.invoke!

  raise "generator return: #{invoker.code}" unless invoker.code.zero?
end
# ok, now I can either build all, or just those in a directory


desc 'run bundler to install/setup wanted gems'
task :setup do
  if File.exist? 'Gemfile'
    system 'bundle install'
  else
    puts 'not possible, no Gemfile found, bad, something is very wrong!'
    exit 1
  end
  # no more tasks, need to reset env
  exit 0
end

desc 'run WEBrick to serve generated pages and Guard for auto generation'
task :serve do
  # pid = fork do
  fork do
    server = WEBrick::HTTPServer.new(DocumentRoot: './html',
                                     BindAddress: '127.0.0.1',
                                     Port: 9090)

    %w[INT TERM].each do |signal|
      Signal.trap(signal) { server.shutdown }
    end

    server.start
    puts 'WEBrick::shutdown via signal'
  end

  Guard.start no_interactions: true
end

desc 'Create a new presentation from the template'
task :new do
  # make a task with nothing to do out of each given arg .....
  # to avoid having rake compalin, what a bad hack to take arguments in a nice way
  ARGV.each { |a| task a.to_sym do ; end }

  if ARGV.length != 2 then
    puts "Task new expects exactly one argument, the name of the new presentation"
    puts "The name will become a folder and can there for be/named/like/one"
    next
  end

  dname = ARGV[1]

  if File.directory? dname then
    puts "Directory #{dname} already exists"
    next
  end

  FileUtils.mkdir_p dname
  FileUtils.copy_entry "vorlage", dname, remove_destination=true

end


# does this work, having it at the end? to run bundle setup?
# or will it still say it can load zip .. TODO test ...
require 'zip'
task zip: [:default] do
  FileUtils.rm_rf PACKZIP
  Zip::File.open(PACKZIP, 'w') do |zipfile|
    Dir["#{PACKDIR}/**/**"].reject { |f| f == PACKZIP }.each do |file|
      zipfile.add(file.sub(PACKDIR + '/’', ''), file)
    end
  end
end

